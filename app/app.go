package app

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	_ "github.com/joho/godotenv/autoload"
	//"servicehub/db/seeds"
)

var (
	router *fiber.App
)

//func seed() {
//	var s seeds.Seed
//	s.SeedCountry()
//}

func StartApplication() {
	//seed()
	router = fiber.New()
	router.Use(cors.New())
	mapURLs()
	_ = router.Listen(":8081")
}
