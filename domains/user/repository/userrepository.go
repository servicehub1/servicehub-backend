package repository

import (
	"errors"
	"servicehub/db"
	"servicehub/domains/user/model"
)

func Find() (*[]model.User, error) {
	var users []model.User
	err := db.Orm.Find(&users)
	if err != nil {
		return nil, err
	}

	if users == nil {
		return nil, errors.New("no users found")
	}

	return &users, nil
}

func Create(user *model.User) error {
	_, err := db.Orm.Insert(user)
	if err != nil {
		return err
	}

	return nil
}

func Get(email string) (*model.User, error) {
	var user model.User

	_, err := db.Orm.Where("email=?", email).Get(&user)

	if err != nil {
		return nil, err
	}

	return &user, nil
}
