package dto

import (
	"servicehub/base"
)

type UserDTO struct {
	base.DTO
	Email    string `json:"email"`
	Password string `json:"password"`
}
