package dto

type CreateUserDTO struct {
	Email    string `json:"email" validate:"format=email & empty=false"`
	Password string `json:"password" validate:"empty=false"`
}
