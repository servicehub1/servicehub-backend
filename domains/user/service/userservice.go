package service

import (
	"servicehub/domains/user/model"
	"servicehub/domains/user/repository"
)

func Find() (*[]model.User, error) {
	return repository.Find()
}

func Create(user *model.User) error {
	return repository.Create(user)
}

func Get(email string) (*model.User, error) {
	return repository.Get(email)
}
