package router

import (
	"github.com/gofiber/fiber/v2"
	"servicehub/domains/user/controller"
)

func RouteUsers(e *fiber.App) {
	e.Get("/users", controller.Find)
	e.Post("/users", controller.Create)
	e.Get("/users/:email", controller.Get)

	//users := e.Group("/users")
	//{
	//	users.GET("", controller.Find)
	//	users.POST("", controller.Create)
	//}
}
