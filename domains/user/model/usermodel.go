package model

import (
	"servicehub/base"
)

type User struct {
	base.Model `xorm:"extends"`
	Email      string `xorm:"varchar(100) unique"`
	Password   string `xorm:"varchar(100)"`
}

func init() {
}
