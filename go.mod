module servicehub

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gofiber/fiber/v2 v2.8.0
	github.com/joho/godotenv v1.3.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.10.1
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/dealancer/validate.v2 v2.1.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
	xorm.io/xorm v1.0.7
)
