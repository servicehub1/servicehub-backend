package db

import (
	"servicehub/domains/user/model"
)

func migrate() {
	_ = Orm.Sync(new(model.User))
}
